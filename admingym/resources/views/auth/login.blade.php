<!DOCTYPE html>
<html lang="es">
<head>
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Pagina acceso - Fitness Admin</title>
    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="../adminlte/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../adminlte/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../adminlte/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../adminlte/css/ace.min.css" />
    <link rel="stylesheet" href="../adminlte/css/ace-rtl.min.css" />
</head>
    <body class="login-layout">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf green"></i>
                                    <span class="red">Fitness</span>
                                    <span class="white" id="id-text2">Application</span>
                                </h1>
                                <h4 class="blue" id="id-company-text">&copy; Company Name</h4>
                            </div>
                            <div class="space-6"></div>
                            <div class="position-relative">
                                <div id="login-box" class="login-box visible widget-box no-border">
                                    <div class="widget-body">
                                        <div class="widget-main">
                                            <h4 class="header blue lighter bigger">
                                                <i class="ace-icon fa fa-coffee green"></i>
                                                Por favor ingrese su información
                                            </h4>
                                            <div class="space-6"></div>
                                            <form method="POST" action="{{ route('login') }}">
                                            @csrf
                                            <fieldset>
                                                <label class="block clearfix" for="email">{{ __('Correo Electronico') }}</label>
                                                <span class="block input-icon input-icon-right">
                                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" placeholder="Correo electronico" required autofocus>
                                                    @if ($errors->has('email'))
                                                    <span class="invalid-feedback" role="alert">
                                                        <strong>{{ $errors->first('email') }}</strong>
                                                    </span>
                                                    @endif
                                                    <i class="ace-icon fa fa-user"></i>
                                                </span>
                                                <label class="block clearfix"  for="password">{{ __('Clave') }}
                                                    <span class="block input-icon input-icon-right">
                                                        <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" placeholder="Clave" required>
                                                        @if ($errors->has('password'))
                                                        <span class="invalid-feedback" role="alert">
                                                            <strong>{{ $errors->first('password') }}</strong>
                                                        </span>
                                                        @endif
                                                        <i class="ace-icon fa fa-lock"></i>
                                                    </span>
                                                </label>
                                                <div class="clearfix">
                                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                                    <label class="inline" for="remember"></label>
                                                    {{__('Recuérdame') }}
                                                    <button type="submit" class="width-35 pull-right btn btn-sm btn-primary">
                                                        <i class="ace-icon fa fa-key"></i>
                                                        <span class="bigger-110">{{ __('Login') }}</span>
                                                    </button>
                                                </div>
                                                <div class="space-4"></div>
                                            </fieldset>
                                            </form>




<div class="toolbar clearfix">
    <div>
        <a href="{{ route('password.request') }}" data-target="#forgot-box" class="forgot-password-link">
            <i class="ace-icon fa fa-arrow-left"></i>
        Olvidé mi contraseña
        </a>
    </div>
    <div>
        <a href="#" data-target="#signup-box" class="user-signup-link">
            Quiero registrarme
            <i class="ace-icon fa fa-arrow-right"></i>
        </a>
    </div>
</div>
</div><!-- /.widget-body -->
</div><!-- /.login-box -->
    <!-- basic scripts -->

    <!--[if !IE]> -->
    <script src="../adminlte/js/jquery-2.1.4.min.js"></script>

    <script type="text/javascript">
        if('ontouchstart' in document.documentElement) document.write("<script src='../adminlte/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
    </script>
</body>
</html>
