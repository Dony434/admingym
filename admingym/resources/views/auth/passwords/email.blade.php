<!DOCTYPE html>
<html lang="es">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta charset="utf-8" />
    <title>Pagina acceso - Fitness Admin</title>
    <meta name="description" content="User login page" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0" />
    <!-- bootstrap & fontawesome -->
    <link rel="stylesheet" href="../adminlte/css/bootstrap.min.css" />
    <link rel="stylesheet" href="../adminlte/font-awesome/4.5.0/css/font-awesome.min.css" />
    <!-- text fonts -->
    <link rel="stylesheet" href="../adminlte/css/fonts.googleapis.com.css" />
    <!-- ace styles -->
    <link rel="stylesheet" href="../adminlte/css/ace.min.css" />
    <link rel="stylesheet" href="../adminlte/css/ace-rtl.min.css" />
    </head>

    <body class="login-layout">
        <div class="main-container">
            <div class="main-content">
                <div class="row">
                    <div class="col-sm-10 col-sm-offset-1">
                        <div class="login-container">
                            <div class="center">
                                <h1>
                                    <i class="ace-icon fa fa-leaf green"></i>
                                    <span class="red">Ace</span>
                                    <span class="white" id="id-text2">Application</span>
                                </h1>
                                <h4 class="blue" id="id-company-text">&copy; Company Name</h4>
                            </div>

                            <div class="space-6"></div>

                            <div class="position-relative">
<!--por aqui login -->          <!--<div id="login-box" class="login-box visible widget-box no-border"> -->
    <div id="forgot-box" class="forgot-box visible widget-box no-border">
                                    @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                    {{ session('status') }}
                                    </div>
                                    @endif
                                    <div class="widget-body">
                                        <div class="widget-main">
<!-- inicio de  resetear contraseña --> 
                                <!--<div id="forgot-box" class="forgot-box widget-box no-border">-->
                                    <!--<div class="widget-body">
                                        <div class="widget-main"> -->
                                            <h4 class="header red lighter bigger">
                                                <i class="ace-icon fa fa-key"></i>
                                                {{ __('Resetear Clave') }}
                                            </h4>

                                            <div class="space-6"></div>
                                            <form method="POST" action="{{ route('password.email') }}">
                                                @csrf
                                                <fieldset>

                                                    <div class="form-group row">
                            <label for="email" class="col-md-6 col-form-label text-md-right">{{ __('Correo Eletronico') }}</label>
                            <div class="col-md-12">
                                <span class="block input-icon input-icon-right">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}" required>
                                <i class="ace-icon fa fa-envelope"></i>
                                </span>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                                                <div class="col-md-12">
                                                    <!-- <div class="clearfix"> -->
                                                        <button type="button" class="width-100 pull-right btn btn-sm btn-danger">
                                                            <i class="ace-icon fa fa-lightbulb-o"></i>
                                                            <span class="bigger-110">
                                                                {{ __('Restablecer contraseña Enviar enlace') }}
                                                            </span>
                                                        </button>
                                                    <!-- </div> -->
                                                </div>
                                                </fieldset>
                                            </form>
                                        </div><!-- /.widget-main -->

                                        <div class="toolbar center">
                                            <!-- <a href="{{ route('login') }}" data-target="#login-box" class="back-to-login-link"> -->
                                            <a href="{{ route('login') }}">Back to login
                                                <i class="ace-icon fa fa-arrow-right"></i>
                                            </a>
                                        </div>
                                    </div><!-- /.widget-body -->
                                </div><!-- /.forgot-box -->

        <!-- basic scripts -->

        <!--[if !IE]> -->
        <script src="../adminlte/js/jquery-2.1.4.min.js"></script>

        <!-- <![endif]-->

        <!--[if IE]>
<script src="assets/js/jquery-1.11.3.min.js"></script>
<![endif]-->
        <script type="text/javascript">
            if('ontouchstart' in document.documentElement) document.write("<script src='../adminlte/js/jquery.mobile.custom.min.js'>"+"<"+"/script>");
        </script>
        <!-- inline scripts related to this page -->
    </body>
</html>
